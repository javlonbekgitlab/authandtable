import React, { useEffect, useState } from 'react'
import styles from './App.module.css'
import { Login } from './components/login'
import { readLocalStorage, removeItemsFromLocalStorage } from './local-storage'
import { Variants } from './pages/variants'

function App() {

  const [logged, setLogged] = useState(false)

  const handleLoginAndLogOut = async ({success}) => {
    setLogged(success)
    !success && removeItemsFromLocalStorage(['ox-token'])
  }

  useEffect(() => {
    readLocalStorage('ox-token') && setLogged(true)
  }, [])

  return (
    <div className = {styles.cont}>
      <div className = {styles.loginCont}>
        {logged 
          ? <Variants onLogOut = {handleLoginAndLogOut}/> 
          : <Login onLogin = {handleLoginAndLogOut}/>
        }
      </div>
    </div>
  )
}

export default App
