import axios from "axios"

export const Api = async({
    host,
    url, 
    data = null, 
    type = 'get', 
    token = null,
    contentType = 'application/json'
}) => {
    var response = {}
    const config = {
        headers: {
            'Content-Type': contentType
        },
    }
    token && (config.headers['Authorization'] = `Bearer ${token}`)
    if (type === 'get') {
        try {
            response = await axios.get(`${host}${url}`, config)
        } catch (error) {
            response = error
            console.log(error)
        }
    }
    if (type === 'post') {
        try {
            response = await axios.post(`${host}${url}`, data, config)
        } catch (error) {
            console.log(error)
            response = error
        }
    }
    return response

}
