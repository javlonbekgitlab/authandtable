import React, { useCallback, useState } from 'react';
import styles from './index.module.css'
import { Paper } from '@mui/material'
import { Input } from '../input';
import { Button } from '../button';
import { Api } from '../../api';
import { apiUrles, ctUrlencoded, host } from '../../api/constants';
import { writeLocalStorage } from '../../local-storage';

export const Login = ({onLogin}) => {

    const [login, setLogin] = useState('')

    const [loading, setLoading] = useState(false)

    const [password, setPassword] = useState('')

    const handleChangePassword = useCallback(({target: {value}}) => setPassword(value), [password, setPassword])
   
    const handleChangeLogin = useCallback(({target: {value}}) => setLogin(value), [login, setLogin])

    const handleLogIn = useCallback( async () => {
        setLoading(true)
        const data = new URLSearchParams({
            _username: login,
            _password: password,
            _subdomain: 'toko'
    
        })
        const resFromLogin = await Api({
            host, 
            url: apiUrles.authCheck, 
            data,
            contentType: ctUrlencoded,
            type: 'post'
        })
        console.log(resFromLogin)
        if (resFromLogin?.status === 200) {
            console.log(resFromLogin.data.token)
            writeLocalStorage({key: 'ox-token', value: resFromLogin.data.token})
            onLogin({success: true})
        } else {
            alert('error')
            onLogin({success: false})
        }
        setLoading(false)
    }, [login, password])

    return (
        <div className = {styles.cont}>
            <Paper
                className = {styles.paper}
                elevation = {3}
            >
                <div className = {styles.auth}>Auth</div>
                <div className = {styles.inputCont}>
                    <Input
                        label = 'login'
                        value = {login}
                        onChange = {handleChangeLogin}
                    />
                </div>
                <div className = {styles.inputCont}>
                    <Input
                        value = {password}
                        label = 'password'
                        onChange = {handleChangePassword}
                    />
                </div>
                <div>
                    <Button
                        loading = {loading}
                        onClick = {handleLogIn}
                    >
                        Log In
                    </Button>
                </div>
            </Paper>
        </div>
    )
}