import React, { useCallback, useEffect, useState } from 'react'
import { Table as Mtable } from '@mui/material'
import TableBody from '@mui/material/TableBody'
import TablePagination from '@mui/material/TablePagination'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import styles from './index.module.css'
import { TableLoader } from '../spinners/tableSpinner'
import { Input } from '../input'
import { Button } from '../button'

export const Table = ({
  tableContent, 
  tableCellTitles, 
  page, 
  setPage, 
  total, 
  size,
  setSize,
  onSearch,
  onClickLogOut
}) => {

  const [loading, setLoading] = useState(false)

  const handleChangePage = useCallback((event, page) => {
    setPage(parseInt(page) + 1)
    setLoading(true)
  }, [page, size])

  const handleChangeRowsPerPage = useCallback(({target: { value }}) => {
    setSize(parseInt(value))
    setLoading(true)
  }, [page, size])

  const handleSearch = ({target: { value }}) => {onSearch(value)}

  useEffect(() => {
    setLoading(false)
  }, [tableContent])

  return (
    <Paper elevation = {3} className = {styles.paper}>
      <div className = {styles.tableHeader}>
        <div className = {styles.tableHeaderItem}>
          <Input
            label = 'search'
            onChange = {handleSearch}
          />
        </div>
        <div className = {styles.tableHeaderItem}>
          <Button
            onClick = {onClickLogOut}
          >
            Log Out
          </Button>
        </div>
      </div>
      <TableContainer className={styles.container}>
        <Mtable 
          className = {styles.table}
          stickyHeader 
          aria-label="sticky table"
          size='medium'
        >
          <TableHead>
            <TableRow>
              {tableCellTitles.map((item) => 
                <TableCell
                  key={item.name}
                  align='center' 
                >
                  {item.name}
                </TableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
            {(Array.isArray(tableContent) && (tableContent.length > 0)) ? tableContent
            .map((itemMain, index) => {
              return (
                <TableRow
                  // hover = {onRowClickParent ? true : false}
                  // className = {classes.tableRow}
                  tabIndex={-1}
                  key = {`${index}1`}
                >
                  {tableCellTitles.map(item => 
                    <TableCell
                      key = {item.key}
                      align='center'
                    >
                      {itemMain[item.key]}
                    </TableCell>
                  )}
                </TableRow>
                )
              }
            ) : <TableRow>
                <div>no data</div>
              </TableRow>
            }
          </TableBody>
        </Mtable>
      </TableContainer>
      <TableLoader start = {loading}/>
      {(total > 15) && <TablePagination
        rowsPerPageOptions = {[5, 10, 25]}
        component = "div"
        count = {total}
        page = {page - 1}
        rowsPerPage = {size}
        labelDisplayedRows = {
          ({ from, to, count }) => {
            return '' + from + '-' + to + ` of ` + count
          }
        }
        SelectProps={{
          inputProps: {
            'aria-label': 'rows per page',
          },
          native: true,
        }}
        // labelRowsPerPage = {lang.table.pagination.rowsPerPage}
        className = {styles.pagination}
        onPageChange = {handleChangePage}
        onRowsPerPageChange = {handleChangeRowsPerPage}
      />}
    </Paper>
  )
}