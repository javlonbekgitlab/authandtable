import { CircularProgress } from '@mui/material/'
import React from 'react'
import styles from './index.module.css'

export const TableLoader = ({start}) => {
    return (
        <div className = {start ? styles.cont : `${styles.cont} ${styles.hide}`}>
            {start && <CircularProgress size = {60}/>}
        </div>
    )
}
