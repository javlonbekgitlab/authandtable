import React, { memo } from 'react';
import { TextField } from '@mui/material'

export const Input = memo(props => {
    return (
        <TextField {...props}/>
    )
})

