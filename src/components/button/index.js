import { Button as ButtonMaterial, CircularProgress } from '@mui/material'
import React, { memo } from 'react'
import styles from './index.module.css'

export const Button = memo(({onClick, children, loading}) => <ButtonMaterial 
    fullWidth 
    variant = 'contained'
    className = {styles.btn}
    onClick = {onClick}
    disabled = {loading}
>
    <div className = {styles.textCont}>
        {children}
        {loading && <div className = {styles.progress}>
            <CircularProgress color="inherit" size={20} />
        </div>}
    </div>
</ButtonMaterial>)