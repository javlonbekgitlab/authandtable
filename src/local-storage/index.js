export const writeLocalStorage = ({key, value}) => localStorage.setItem(key, value)

export const readLocalStorage = key => localStorage.getItem(key)

export const removeItemsFromLocalStorage = keys => keys.map(key => localStorage.removeItem(key))
