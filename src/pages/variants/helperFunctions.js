import { Api } from "../../api"
import { apiUrles, host } from "../../api/constants"


export const getVariants = async ({page = 1, size = 5, token}) => {
    const res = await Api({
        host, 
        url: `${apiUrles.variatons}?page=${page}&size=${size}`, 
        // contentType: ctUrlencoded,
        token,
        type: 'get'
    })
    console.log(res)
    if (res?.status === 200) {
        return res.data
    } else {
        alert('error')
        return false
    }
}

export const compareWithKeys = ({a,b,key}) => {
    if (a[key] > b[key]) {
      return 1
    }
    if (a[key] < b[key]) {
      return -1
    }
    return 0
  }