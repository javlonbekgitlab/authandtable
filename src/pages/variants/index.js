import React, { useEffect, useState } from 'react'
import { Table } from '../../components/table'
import { readLocalStorage } from '../../local-storage'
import { tableTitles } from './constants'
import { compareWithKeys, getVariants } from './helperFunctions'
import styles from './index.module.css'

export const Variants = ({onLogOut}) => {
// solution with hooks
    const [data, setData] = useState([])

    const [staticData, setStaticData] = useState([])

    const [page, setPage] = useState(1)

    const [size ,setSize] = useState(5)

    const [total, setTotal] = useState(0)

    const getData = async () => {
        const token = readLocalStorage('ox-token')
        if (token) {
            const newData = await getVariants({page, size, token}) 
            if (newData) {
                setData(newData.items)
                setStaticData(newData.items)
                setTotal(newData.total_count)
            } else {
                setData([])
                setStaticData([])
                setTotal(0)
                onLogOut({success: false})
            }
        } else {
            onLogOut({success: false})
        }
    }

    const handleSearch = value => {
        if (value) {
            const searchingData = []
            for(let index = 0; index < staticData.length; index++) {
                for(let titleIndex = 0; titleIndex < tableTitles.length; titleIndex++) {
                    console.log(tableTitles[titleIndex]['key'])
                    if (`${staticData[index][tableTitles[titleIndex]['key']]}`.toLowerCase().includes(value.toLowerCase())) {
                        searchingData.push({
                            ...staticData[index],
                            index: staticData[index]['name'].indexOf(value),
                        })
                    } 
                }
            }
            const sortedName = searchingData.sort((a,b) => compareWithKeys({a, b, key: 'name'}))
            const sorted = sortedName.sort((a,b) => compareWithKeys({a, b, key: 'index'}))
            setData(sorted)
        } else {
            setData(staticData)
        }
    } 

    const handleLogOut = () => onLogOut({success: false})

    useEffect(getData, [])

    useEffect(getData, [page, size])

    return (
        <div className = {styles.cont}>
            <Table
                tableContent = {data}
                tableCellTitles = {tableTitles}
                getData = {null}
                page = {page}
                setPage = {setPage}
                total = {total}
                size = {size}
                setSize = {setSize}
                onSearch = {handleSearch}
                onClickLogOut = {handleLogOut}
            />
        </div>
    )
}