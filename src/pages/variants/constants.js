export const tableTitles = [
    {
        name: 'id',
        key: 'id'
    },
    {
        name: 'supplier',
        key: 'supplier'
    },
    {
        name: 'barcode',
        key: 'barcode'
    },
    {
        name: 'name',
        key: 'name'
    },
    {
        name: 'lastUpdateTime',
        key: 'lastUpdateTime'
    }
]